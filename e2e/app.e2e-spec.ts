import { MdKartyPage } from './app.po';

describe('md-karty App', () => {
  let page: MdKartyPage;

  beforeEach(() => {
    page = new MdKartyPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
