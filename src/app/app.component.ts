import {DatePipe} from '@angular/common';
import {Component} from '@angular/core';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import {ArrayOfArray} from './model/array-of-array.model';
import {BankEntry} from './model/bank-entry.model';
import {CardTopup} from './model/card-topup.model';
import {CardDataColumn, Column} from './model/column.enum';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  public cards: Array<CardTopup>;
  public bankEntries: Array<BankEntry>;
  public fileNameWithAmounts: string;
  public fileNameWithoutAmounts: string;
  public fileNameWithBankAccount: string;

  public errorMessage: string;

  private bankFilePrefix = `Numer;Kwota\r\nkarty*;zasilenia*`;

  constructor(private datePipe: DatePipe) {
    const currentDate = this.datePipe.transform(new Date(), 'dd-MM-yyyy');

    this.fileNameWithAmounts = 'karty_' + currentDate
    this.fileNameWithoutAmounts = 'czyste_karty_' + currentDate;
    this.fileNameWithBankAccount = 'plik_bank_' + currentDate

  }

  public readTopupOrders(evt: any) {
    /* wire up file reader */
    const target: DataTransfer = <DataTransfer>(evt.target);
    if (target.files.length !== 1) {
      throw new Error('Cannot use multiple files');
    }
    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
      /* read workbook */
      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, {type: 'binary'});

      /* grab first sheet */
      const wsname: string = wb.SheetNames[0];
      const ws: XLSX.WorkSheet = wb.Sheets[wsname];

      /* save data */
      const dataFromSheet = <ArrayOfArray>(XLSX.utils.sheet_to_json(ws, {header: 1}));
      this.readCardsFromWorkbook(dataFromSheet);
    };
    reader.readAsBinaryString(target.files[0]);
  }

  public readCardsFile(evt: any) {
    this.errorMessage = '';
    this.bankEntries = new Array<BankEntry>();
    /* wire up file reader */
    const target: DataTransfer = <DataTransfer>(evt.target);
    if (target.files.length !== 1) {
      throw new Error('Cannot use multiple files');
    }
    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
      const bstr: string = e.target.result;
      const cardsArray = bstr.split(/[\r\n]+/);

      cardsArray
        .map(row => row.split(' '))
        .filter(row => row[0].length)
        .forEach(bankEntry => {
          this.bankEntries.push({
            cardData: bankEntry[0],
            bankAccount: this.getBankAccountFromCardData(bankEntry[0]),
            amount: bankEntry[1]
          });
        });
    };
    reader.readAsText(target.files[0]);
  }

  private getBankAccountFromCardData(cardData: string): string {
    const bankAccount = cardData.split('|')[CardDataColumn.BANK_ACCOUNT]

    if (!bankAccount.startsWith('4410')) {
      const errorMessage =
        `Numer karty nie zaczyna się od liczb '4410'!: ${bankAccount}`;
      this.errorMessage = errorMessage;
      throw new RangeError(errorMessage);
    }

    return bankAccount;
  }

  public readCardsFromWorkbook(dataFromSheet: ArrayOfArray): void {
    this.cards = new Array<CardTopup>();
    this.errorMessage = undefined;
    dataFromSheet
      .filter(datRow => datRow.length > 19)
      .filter(dataRow => this.isNotHeader(dataRow))
      .forEach(dataRow => {
        for (let i = 0; i < dataRow[Column.CARDS_COUNT]; i++) {
          const cardNumber = dataRow[Column.CARD_NO_START + i];

          if (!cardNumber) {
            const errorMessage = `Brak ${i + 1}ej karty <br/>
            Numer zamówienia: ${dataRow[Column.ORDER_NUMBER]}<br/>
            Klient: ${dataRow[Column.FIRST_NAME]} ${dataRow[Column.LAST_NAME]}`;
            this.errorMessage = errorMessage;
            throw new RangeError(errorMessage);
          }

          this.cards.push({
            number: cardNumber,
            amount: dataRow[Column.TOPUP_AMOUNT]
          });
        }
      });
  }

  public processCards(): void {
    this.errorMessage = undefined;

    if (this.bankEntries.length < this.cards.length) {
      const errorMessage =
        `Plik z kartami ma mniej wpisów (${this.bankEntries.length}) od wymaganej ilości kart:  ${this.cards.length}!`;
      this.errorMessage = errorMessage;
      throw new RangeError(errorMessage);
    }

    this.cards
      .forEach(card => {
        const entriesForCard = this.bankEntries
          .filter(bankEntry => bankEntry.cardData.includes(card.number));

        if (entriesForCard.length !== 1) {
          let errorMessage;
          if (entriesForCard.length === 0) {
            errorMessage = `Nie znaleziono karty ${card.number} w pliku bankowym!`;
          }
          if (entriesForCard.length > 1) {
            errorMessage = `Dla karty ${card.number} znaleziono ${entriesForCard.length} wpisów w pliku bankowym!`;
          }
          this.errorMessage = errorMessage;
          throw new RangeError(errorMessage);
        }

        const cardBankEntry = entriesForCard[0];

        if (cardBankEntry.amount) {
          const errorMessage = `Dla karty ${card.number} znaleziono wpis z wartością ${cardBankEntry.amount}zł!`;
          this.errorMessage = errorMessage;
          throw new RangeError(errorMessage);
        }

        cardBankEntry.amount = card.amount;
      });

    const bankEntriesWithAmount = this.bankEntries
      .filter(entry => entry.amount);

    const cardBankEntriesWithAmount = bankEntriesWithAmount
      .map(entry => entry.cardData + ' ' + entry.amount)
      .join('\r\n');

    const cardsBankFile = this.bankFilePrefix + '\r\n' + cardBankEntriesWithAmount;

    this.saveToFileSystem(this.fileNameWithAmounts + '.txt', cardsBankFile);

    const cardBankEntriesWithoutAmount = this.bankEntries
      .filter(entry => !entry.amount)
      .map(entry => entry.cardData)
      .join('\r\n');

    this.saveToFileSystem(this.fileNameWithoutAmounts + '.txt', cardBankEntriesWithoutAmount);

    const bankAccountEntriesWithAmount = bankEntriesWithAmount
      .map(entry => entry.bankAccount + ';' + entry.amount)
      .join('\r\n');

    this.saveToFileSystem(this.fileNameWithBankAccount + '.txt', bankAccountEntriesWithAmount);
  }

  public isProcessDisabled(): boolean {
    return !this.cards
      || this.cards.length === 0
      || !this.bankEntries
      || this.bankEntries.length === 0;
  }

  public getTotalTopupAmonut(): number {
    return this.cards
      .reduce((prev: number, curr: CardTopup) => {
        return prev + Number(curr.amount);
      }, 0);
  }

  public getEmptyBankEntriesCount(): number {
    return this.bankEntries
      .filter(entry => !entry.amount)
      .length;
  }

  private saveToFileSystem(fileName: string, text: any) {
    const contentDispositionHeader: string = 'attachment; filename=' + fileName;
    const parts: string[] = contentDispositionHeader.split(';');
    const filename = parts[1].split('=')[1];
    const blob = new Blob([text], {type: 'text/plain'});
    FileSaver.saveAs(blob, filename);
  }

  private isNotHeader(dataRow: Array<any>): boolean {
    return dataRow[1] !== 'Program';
  }
}
