export interface CardTopup {
  number: string;
  amount: string;
}
