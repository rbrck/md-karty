export interface BankEntry {
  cardData: string;
  bankAccount: string;
  amount: string;
}
