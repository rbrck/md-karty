export enum Column {
  AMOUNT_ORDERED = 8,
  CARDS_COUNT = 18,
  TOPUP_AMOUNT = 19,
  CARD_NO_START = 20,
  FIRST_NAME = 8,
  LAST_NAME = 9,
  ORDER_NUMBER = 0,
}

export enum CardDataColumn {
  BANK_ACCOUNT = 6,
}
